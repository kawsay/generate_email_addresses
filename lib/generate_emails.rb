require 'dotenv/load'
require 'pry'
require 'net/ldap'
require 'i18n'

# Set locales
I18n.config.available_locales = :fr
I18n.locale = :fr

# Replace characters like `[éôà]` with RFC 5322 compliant ones
def sanitize_name!(name)
  I18n.transliterate name
end

def format_email_address(firstname, lastname)
  firstname_arr = firstname.split(/[\s\-\_\']/)
  
  formatted_firstname = if firstname_arr.size > 1
    firstname_arr.map.with_index do |firstname, index|
      firstname.downcase!
      
      index == (firstname_arr.size - 1) ? firstname : firstname[0]
    end.join
  else
    firstname
  end

  formatted_lastname = lastname.split(' ').join

  "#{formatted_firstname}.#{formatted_lastname}@sdis21.org"
end

def run
  emails    = []
  ldap      = Net::LDAP.new
  ldap.host = ENV['LDAP_HOST']
  ldap.port = ENV['LDAP_PORT']
  ldap.auth ENV['LDAP_USERNAME'], ENV['LDAP_PASSWORD']

  if ldap.bind
    treebase   = ENV['LDAP_BASE']
    attributes = ['sn', 'givenname', 'mail']
    filter     = Net::LDAP::Filter.construct(ENV['LDAP_OU_FILTER_1']) |
                 Net::LDAP::Filter.construct(ENV['LDAP_OU_FILTER_2']) |
                 Net::LDAP::Filter.construct(ENV['LDAP_OU_FILTER_3'])

    ldap.search(base: treebase, attributes: attributes, filter: filter) do |entry|
      firstname = sanitize_name!(entry[:givenname][0].downcase) unless entry[:givenname][0].nil?
      lastname  = sanitize_name!(entry[:sn][0].downcase)        unless entry[:sn][0].nil?

      email = format_email_address(firstname, lastname)

      # Display homonyms
      if emails.include?(email)
        puts email
        emails << email
      else
        emails << email
      end
    end
    binding.pry
  else
    puts '[-] Cannot bind, possibly wrong credentials'
  end
end

run
